//
//  SecondViewController.swift
//  TestSevenHugs
//
//  Created by Thomas Droin on 20/03/2017.
//  Copyright © 2017 Thomas Droin. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMedia
import MediaPlayer

class SoundViewController: UIViewController, SoundMapperDelegate {

    var currentMusicPlayer : MPMusicPlayerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: SOUNDMAPPER DELEGATE
    func albumDidChange(album: String) {
        
    }
    func artistDidChange(artist: String) {
        
    }
    func playlistDidChange() {
        
    }
}

