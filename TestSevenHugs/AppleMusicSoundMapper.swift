//
//  AppleMusicSoundMapper.swift
//  TestSevenHugs
//
//  Created by Thomas Droin on 20/03/2017.
//  Copyright © 2017 Thomas Droin. All rights reserved.
//

import Foundation

class AppleMusicSoundMapper: NSObject,SoundMapper {
    
    var delegate : SoundMapperDelegate?
    
    //MARK: INIT
    
    init(delegate : SoundMapperDelegate) {
        super.init()
        self.delegate = delegate
    }
    
    
    //SOUNDMAPPER DELEGATE
    func play() {
        // PLAY
    }
    
    func pause() {
        // PAUSE
    }
    func next() {
        // NEXT SONG
        
    }
    func prev() {
        //PREV SONG
    }
    
    
    //EX WHEN APPLE MUSIC DELEGATE TELL US WE JUST HAVE TO CALL
    
    func appleMusicAlbumChange (album : String){
        self.delegate?.albumDidChange(album: album)
    }
    
    
}
