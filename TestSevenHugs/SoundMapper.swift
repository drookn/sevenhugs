//
//  SoundMapper.swift
//  TestSevenHugs
//
//  Created by Thomas Droin on 20/03/2017.
//  Copyright © 2017 Thomas Droin. All rights reserved.
//

import Foundation

//Protocol permettant de piloter le fournisseur de musique
//MARK : SOUND MAPPER PROTOCOL
protocol SoundMapper {
    func play()
    func pause()
    func next()
    func prev()
}

//MARK : SOUND MAPPER DELEGATE PROTOCOL

protocol SoundMapperDelegate {
    func artistDidChange(artist: String)
    func albumDidChange(album: String)
    func playlistDidChange()
}
